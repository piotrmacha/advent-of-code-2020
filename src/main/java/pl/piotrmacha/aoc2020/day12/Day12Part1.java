package pl.piotrmacha.aoc2020.day12;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;

public class Day12Part1 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<String> instructions = InputUtils.splitToStrings(input);

        Vector2 position = new Vector2(0, 0);
        Direction direction = Direction.EAST;

        for (String instruction : instructions) {
            char op = instruction.charAt(0);
            int arg = Integer.parseInt(instruction.substring(1));

            switch (op) {
                case 'N':
                    position = Direction.NORTH.move(position, arg);
                    break;
                case 'S':
                    position = Direction.SHOUT.move(position, arg);
                    break;
                case 'E':
                    position = Direction.EAST.move(position, arg);
                    break;
                case 'W':
                    position = Direction.WEST.move(position, arg);
                    break;
                case 'L':
                    direction = direction.rotate(-arg);
                    break;
                case 'R':
                    direction = direction.rotate(arg);
                    break;
                case 'F':
                    position = direction.move(position, arg);
            }
        }

        int distance = Math.abs(position.x) + Math.abs(position.y);

        return String.valueOf(distance);
    }

    private static class Vector2 {
        public int x = 0;
        public int y = 0;

        public Vector2(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private enum Direction {
        NORTH,
        EAST,
        SHOUT,
        WEST;

        Direction rotate(int degrees) {
            int forwardOrdinal = degrees / 90;
            Direction[] values = values();
            return values[Math.floorMod(ordinal() + forwardOrdinal, values.length)];
        }

        Vector2 move(Vector2 origin, int units) {
            switch (this) {
                case NORTH:
                    return new Vector2(origin.x, origin.y + units);
                case SHOUT:
                    return new Vector2(origin.x, origin.y - units);
                case EAST:
                    return new Vector2(origin.x + units, origin.y);
                case WEST:
                    return new Vector2(origin.x - units, origin.y);
            }
            throw new RuntimeException("Invalid operation");
        }
    }
}