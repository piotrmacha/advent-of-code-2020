package pl.piotrmacha.aoc2020.day9;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;

public class Day9Part2 implements Function<String, String> {
    private final static int PREAMBLE_LENGTH = 25;

    @Override
    public String apply(String input) {
        List<Long> stream = InputUtils.splitToLongs(input);

        long number = findInvalidNumber(stream);

        for (int i = 0; i < stream.size(); ++i) {
            Long head = stream.get(i);

            long accumulator = head;
            long min = head;
            long max = head;

            for (int j = i + 1; j < stream.size(); ++j) {
                Long tail = stream.get(j);

                accumulator += tail;

                if (min > tail) {
                    min = tail;
                }
                if (max < tail) {
                    max = tail;
                }

                if (accumulator == number) {
                    long result = min + max;
                    return String.valueOf(result);
                }
            }
        }

        throw new RuntimeException("Result can not be found");
    }

    private long findInvalidNumber(List<Long> stream) {
        for (int i = PREAMBLE_LENGTH; i < stream.size(); ++i) {
            long current = stream.get(i);
            boolean valid = false;

            check_outer:
            for (int j = i - PREAMBLE_LENGTH; j < i; ++j) {
                for (int k = j + 1; k < i; ++k) {
                    long a = stream.get(j);
                    long b = stream.get(k);
                    if (current == a + b) {
                        valid = true;
                        break check_outer;
                    }
                }
            }

            if (!valid) {
                return current;
            }
        }

        throw new RuntimeException("Result can not be found");
    }
}