package pl.piotrmacha.aoc2020.day9;

import pl.piotrmacha.aoc2020.InputUtils;
import pl.piotrmacha.aoc2020.day8.Day8VM;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class Day9Part1 implements Function<String, String> {
    private final static int PREAMBLE_LENGTH = 25;

    @Override
    public String apply(String input) {
        List<Long> stream = InputUtils.splitToLongs(input);

        for (int i = PREAMBLE_LENGTH; i < stream.size(); ++i) {
            long current = stream.get(i);
            boolean valid = false;

            check_outer:
            for (int j = i - PREAMBLE_LENGTH; j < i; ++j) {
                for (int k = j + 1; k < i; ++k) {
                    long a = stream.get(j);
                    long b = stream.get(k);
                    if (current == a + b) {
                        valid = true;
                        break check_outer;
                    }
                }
            }

            if (!valid) {
                return String.valueOf(current);
            }
        }

        throw new RuntimeException("Result can not be found");
    }
}