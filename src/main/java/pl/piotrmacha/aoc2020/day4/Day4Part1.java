package pl.piotrmacha.aoc2020.day4;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class Day4Part1 implements Function<String, String> {
    private static final String[] REQUIRED_FIELDS = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

    @Override
    public String apply(String input) {
        List<List<String>> entries = Arrays.stream(input.split("\n\n"))
                .map(entry -> entry.replace("\n", " "))
                .map(entry -> asList(entry.split("\\s+")))
                .collect(toList());

        long result = entries.stream()
                .filter(entry -> {
                    for (String requiredField : REQUIRED_FIELDS) {
                        boolean match = entry.stream()
                                .map(field -> field.split(":")[0])
                                .anyMatch(field -> field.equals(requiredField));
                        if (!match) {
                            return false;
                        }
                    }
                    return true;
                })
                .count();

        return String.valueOf(result);
    }
}