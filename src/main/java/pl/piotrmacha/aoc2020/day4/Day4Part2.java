package pl.piotrmacha.aoc2020.day4;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class Day4Part2 implements Function<String, String> {
    private static final String[] REQUIRED_FIELDS = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"};

    @Override
    public String apply(String input) {
        List<Passport> entries = Arrays.stream(input.split("\n\n"))
                .map(entry -> entry.replace("\n", " "))
                .map(entry -> asList(entry.split("\\s+")))
                .map(Passport::new)
                .collect(toList());

        long result = entries.stream()
                .filter(Passport::valid)
                .count();

        return String.valueOf(result);
    }

    private static class Passport {
        private final Map<String, String> fields = new HashMap<>();

        public Passport(List<String> fieldsData) {
            for (String fieldData : fieldsData) {
                String[] parts = fieldData.split(":");
                fields.put(parts[0], parts[1]);
            }
        }

        boolean valid() {
            for (String requiredField : REQUIRED_FIELDS) {
                if (!fields.containsKey(requiredField)) {
                    return false;
                }
            }

            int byr = Integer.parseInt(fields.get("byr"));
            if (byr < 1920 || byr > 2002) {
                return false;
            }

            int iyr = Integer.parseInt(fields.get("iyr"));
            if (iyr < 2010 || iyr > 2020) {
                return false;
            }

            int eyr = Integer.parseInt(fields.get("eyr"));
            if (eyr < 2020 || eyr > 2030) {
                return false;
            }

            String hgt = fields.get("hgt");
            if (!hgt.matches("\\d+(cm|in)")) {
                return false;
            }
            String hgtUnit = hgt.substring(hgt.length() - 2);
            int hgtValue = Integer.parseInt(hgt.substring(0, hgt.length() - 2));
            if ("cm".equals(hgtUnit) && (hgtValue < 150 || hgtValue > 193)) {
                return false;
            }
            if ("in".equals(hgtUnit) && (hgtValue < 59 || hgtValue > 76)) {
                return false;
            }

            String hcl = fields.get("hcl");
            if (!hcl.matches("#[0-9a-f]{6}")) {
                return false;
            }

            String ecl = fields.get("ecl");
            if (!ecl.matches("amb|blu|brn|gry|grn|hzl|oth")) {
                return false;
            }

            String pid = fields.get("pid");
            if (!pid.matches("[0-9]{9}")) {
                return false;
            }

            return true;
        }
    }
}