package pl.piotrmacha.aoc2020.day2;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day2Part1 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<String> policies = InputUtils.splitToStrings(input);

        long result = policies.stream()
                .filter(policyAndPassword -> {
                    Matcher matcher = Pattern.compile("(\\d+)-(\\d+) ([a-z]): ([a-z]+)").matcher(policyAndPassword);
                    if (matcher.find()) {
                        int min = Integer.parseInt(matcher.group(1));
                        int max = Integer.parseInt(matcher.group(2));
                        char letter = matcher.group(3).charAt(0);
                        String password = matcher.group(4);

                        long occurrences = password.chars().filter(c -> c == letter).count();

                        return occurrences >= min && occurrences <= max;
                    }

                    throw new RuntimeException("Invalid input");
                })
                .count();

        return String.valueOf(result);
    }
}