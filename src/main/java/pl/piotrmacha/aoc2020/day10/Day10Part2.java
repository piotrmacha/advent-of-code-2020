package pl.piotrmacha.aoc2020.day10;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.*;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

public class Day10Part2 implements Function<String, String> {
    private final Map<CounterCacheKey, Long> counterCache = new HashMap<>();

    @Override
    public String apply(String input) {
        List<Long> adapters = InputUtils.splitToLongs(input).stream()
                .sorted()
                .collect(toList());

        long target = adapters.get(adapters.size() - 1) + 3;

        long result = countPossibleArrangements(adapters, 0, 0, target);

        return String.valueOf(result);
    }

    private long countPossibleArrangements(List<Long> adapters, int fromIndex, long source, long target) {
        // This function is called multiple times with same arguments, so we cache its' result to drastically
        // reduce the amount of branches in recursion and provide result in sub-second time.
        CounterCacheKey cacheKey = new CounterCacheKey(fromIndex, source, target);
        if (counterCache.containsKey(cacheKey)) {
            return counterCache.get(cacheKey);
        }

        if (target - source <= 3) {
            counterCache.put(cacheKey, 1L);
            return 1;
        }

        List<Integer> choiceIndices = new ArrayList<>();
        for (int i = fromIndex; i < adapters.size(); ++i) {
            long adapter = adapters.get(i);
            if (adapter - source > 3) {
                break;
            }
            choiceIndices.add(i);
        }

        long accumulator = 0;
        for (int choiceIndex : choiceIndices) {
            long adapter = adapters.get(choiceIndex);
            accumulator += countPossibleArrangements(adapters, choiceIndex + 1, adapter, target);
        }

        counterCache.put(cacheKey, accumulator);
        return accumulator;
    }

    private static class CounterCacheKey {
        public int fromIndex;
        public long source;
        public long target;

        public CounterCacheKey(int fromIndex, long source, long target) {
            this.fromIndex = fromIndex;
            this.source = source;
            this.target = target;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            CounterCacheKey that = (CounterCacheKey) o;
            return fromIndex == that.fromIndex && source == that.source && target == that.target;
        }

        @Override
        public int hashCode() {
            return Objects.hash(fromIndex, source, target);
        }
    }
}