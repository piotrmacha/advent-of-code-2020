package pl.piotrmacha.aoc2020.day10;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class Day10Part1 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<Long> adapters = InputUtils.splitToLongs(input).stream()
                .sorted()
                .collect(toList());

        adapters.add(adapters.get(adapters.size() - 1) + 3);

        long differencesOf1Jolt = 0;
        long differencesOf2Jolts = 0;
        long currentJoltage = 0;

        for (Long adapter : adapters) {
            if (adapter - currentJoltage == 1) {
                differencesOf1Jolt++;
            }
            if (adapter - currentJoltage == 3) {
                differencesOf2Jolts++;
            }
            currentJoltage = adapter;
        }

        return String.valueOf(differencesOf1Jolt * differencesOf2Jolts);
    }
}