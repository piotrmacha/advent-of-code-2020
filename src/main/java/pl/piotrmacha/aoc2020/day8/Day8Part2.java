package pl.piotrmacha.aoc2020.day8;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class Day8Part2 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<String> program = InputUtils.splitToStrings(input);
        int changeIndex = 0;

        Day8VM vm = new Day8VM(program);
        while (!vm.terminated) {
            List<String> modifiedProgram = new ArrayList<>(program);
            for (int i = changeIndex; i < modifiedProgram.size(); ++i) {
                String line = modifiedProgram.get(i);
                if (line.startsWith("nop")) {
                    modifiedProgram.set(i, line.replace("nop", "jmp"));
                    changeIndex = i + 1;
                    break;
                } else if (line.startsWith("jmp")) {
                    modifiedProgram.set(i, line.replace("jmp", "nop"));
                    changeIndex = i + 1;
                    break;
                }
            }

            vm.reset(modifiedProgram);

            Set<Integer> visitedInstructions = new HashSet<>();
            while (!visitedInstructions.contains(vm.ip)) {
                visitedInstructions.add(vm.ip);
                vm.next();
            }
        }

        return String.valueOf(vm.accumulator);
    }
}