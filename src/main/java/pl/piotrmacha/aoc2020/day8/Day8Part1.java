package pl.piotrmacha.aoc2020.day8;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.*;
import java.util.function.Function;

public class Day8Part1 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<String> program = InputUtils.splitToStrings(input);
        Day8VM vm = new Day8VM(program);

        Set<Integer> visitedInstructions = new HashSet<>();
        while (!visitedInstructions.contains(vm.ip)) {
            visitedInstructions.add(vm.ip);
            vm.next();
        }

        return String.valueOf(vm.accumulator);
    }
}