package pl.piotrmacha.aoc2020.day8;

import java.util.List;

public class Day8VM {
    public int ip = 0;
    public int accumulator = 0;
    public boolean terminated = false;

    private List<String> program;

    public Day8VM(List<String> program) {
        this.program = program;
    }

    public void reset(List<String> program) {
        ip = 0;
        accumulator = 0;
        terminated = false;
        this.program = program;
    }

    public void next() {
        if (program.size() <= ip) {
            terminated = true;
            return;
        }

        String[] instruction = program.get(ip).split(" ");
        String op = instruction[0];
        int arg = Integer.parseInt(instruction[1]);

        if ("nop".equals(op)) {
            ip++;
        } else if ("acc".equals(op)) {
            accumulator += arg;
            ip++;
        } else if ("jmp".equals(op)) {
            ip += arg;
        }
    }
}
