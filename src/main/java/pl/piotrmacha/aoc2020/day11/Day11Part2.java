package pl.piotrmacha.aoc2020.day11;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

public class Day11Part2 implements Function<String, String> {
    @Override
    public String apply(String input) {
        Grid grid = new Grid(input);

        while (grid.isChanged()) {
            grid.step((x, y, type) -> {
                switch (type) {
                    case EMPTY: {
                        List<CellType> seenCells = new ArrayList<>();
                        seenCells.add(grid.findSeatRayCast(x, y, 0, 1));
                        seenCells.add(grid.findSeatRayCast(x, y, 1, 1));
                        seenCells.add(grid.findSeatRayCast(x, y, 1, 0));
                        seenCells.add(grid.findSeatRayCast(x, y, 1, -1));
                        seenCells.add(grid.findSeatRayCast(x, y, 0, -1));
                        seenCells.add(grid.findSeatRayCast(x, y, -1, -1));
                        seenCells.add(grid.findSeatRayCast(x, y, -1, 0));
                        seenCells.add(grid.findSeatRayCast(x, y, -1, 1));

                        long occupiedSeats = seenCells.stream()
                                .filter(cell -> cell.equals(CellType.OCCUPIED))
                                .count();

                        if (occupiedSeats == 0) {
                            return CellType.OCCUPIED;
                        }
                    }
                    case OCCUPIED: {
                        List<CellType> seenCells = new ArrayList<>();
                        seenCells.add(grid.findSeatRayCast(x, y, 0, 1));
                        seenCells.add(grid.findSeatRayCast(x, y, 1, 1));
                        seenCells.add(grid.findSeatRayCast(x, y, 1, 0));
                        seenCells.add(grid.findSeatRayCast(x, y, 1, -1));
                        seenCells.add(grid.findSeatRayCast(x, y, 0, -1));
                        seenCells.add(grid.findSeatRayCast(x, y, -1, -1));
                        seenCells.add(grid.findSeatRayCast(x, y, -1, 0));
                        seenCells.add(grid.findSeatRayCast(x, y, -1, 1));

                        long occupiedSeats = seenCells.stream()
                                .filter(cell -> cell.equals(CellType.OCCUPIED))
                                .count();

                        if (occupiedSeats >= 5) {
                            return CellType.EMPTY;
                        }
                    }
                }
                return type;
            });
        }

        int result = grid.countOfType(CellType.OCCUPIED);

        return String.valueOf(result);
    }

    private enum CellType {
        EMPTY,
        OCCUPIED,
        FLOOR
    }

    private interface CellFunction {
        CellType apply(int x, int y, CellType type);
    }

    private static class Grid {
        private List<List<CellType>> grid = new ArrayList<>();
        private boolean changed = true;

        public Grid(String input) {
            List<String> lines = InputUtils.splitToStrings(input);
            for (String line : lines) {
                List<CellType> row = new ArrayList<>();
                for (char c : line.trim().toCharArray()) {
                    switch (c) {
                        case 'L':
                            row.add(CellType.EMPTY);
                            break;
                        case '#':
                            row.add(CellType.OCCUPIED);
                            break;
                        default:
                            row.add(CellType.FLOOR);
                    }
                }
                grid.add(row);
            }
        }

        public CellType at(int x, int y) {
            if (grid.size() > y && y >= 0) {
                List<CellType> row = grid.get(y);
                if (row.size() > x && x >= 0) {
                    return row.get(x);
                }
            }
            return CellType.FLOOR;
        }

        public CellType findSeatRayCast(int x, int y,  int dx, int dy) {
            int cx = x + dx;
            int cy = y + dy;
            while (cy < grid.size() && cy >= 0 && cx < grid.get(0).size() && cx >= 0) {
                CellType cell = at(cx, cy);

                if (!cell.equals(CellType.FLOOR)) {
                    return cell;
                }

                cx += dx;
                cy += dy;
            }
            return CellType.FLOOR;
        }

        public int countOfType(CellType type) {
            int accumulator = 0;
            for (List<CellType> row : grid) {
                for (CellType cell : row) {
                    if (cell.equals(type)) {
                        accumulator++;
                    }
                }
            }
            return accumulator;
        }

        public void step(CellFunction function) {
            changed = false;
            List<List<CellType>> newGrid = new ArrayList<>();
            for (int y = 0; y < grid.size(); ++y) {
                List<CellType> row = grid.get(y);
                newGrid.add(new ArrayList<>());
                for (int x = 0; x < row.size(); ++x) {
                    CellType cell = row.get(x);
                    CellType result = function.apply(x, y, cell);
                    newGrid.get(y).add(result);
                    if (!cell.equals(result)) {
                        changed = true;
                    }
                }
            }
            grid = newGrid;
        }

        public boolean isChanged() {
            return changed;
        }

        public void print() {
            for (List<CellType> row : grid) {
                for (CellType cell : row) {
                    switch (cell) {
                        case EMPTY:
                            System.out.print('L');
                            break;
                        case OCCUPIED:
                            System.out.print('#');
                            break;
                        case FLOOR:
                            System.out.print('.');
                            break;
                    }
                }
                System.out.print('\n');
            }
        }
    }
}