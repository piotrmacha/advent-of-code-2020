package pl.piotrmacha.aoc2020.day11;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

public class Day11Part1 implements Function<String, String> {
    @Override
    public String apply(String input) {
        Grid grid = new Grid(input);

        while (grid.isChanged()) {
            grid.step((x, y, type) -> {
                switch (type) {
                    case EMPTY: {
                        if (0 == grid.countAdjacentOfType(x, y, CellType.OCCUPIED)) {
                            return CellType.OCCUPIED;
                        }
                    }
                    case OCCUPIED: {
                        if (4 <= grid.countAdjacentOfType(x, y, CellType.OCCUPIED)) {
                            return CellType.EMPTY;
                        }
                    }
                }
                return type;
            });
        }

        int result = grid.countOfType(CellType.OCCUPIED);

        return String.valueOf(result);
    }

    private enum CellType {
        EMPTY,
        OCCUPIED,
        FLOOR
    }

    private interface CellFunction {
        CellType apply(int x, int y, CellType type);
    }

    private static class Grid {
        private List<List<CellType>> grid = new ArrayList<>();
        private boolean changed = true;

        public Grid(String input) {
            List<String> lines = InputUtils.splitToStrings(input);
            for (String line : lines) {
                List<CellType> row = new ArrayList<>();
                for (char c : line.trim().toCharArray()) {
                    switch (c) {
                        case 'L':
                            row.add(CellType.EMPTY);
                            break;
                        case '#':
                            row.add(CellType.OCCUPIED);
                            break;
                        default:
                            row.add(CellType.FLOOR);
                    }
                }
                grid.add(row);
            }
        }

        public CellType at(int x, int y) {
            if (grid.size() > y && y >= 0) {
                List<CellType> row = grid.get(y);
                if (row.size() > x && x >= 0) {
                    return row.get(x);
                }
            }
            return CellType.FLOOR;
        }

        public int countOfType(CellType type) {
            int accumulator = 0;
            for (List<CellType> row : grid) {
                for (CellType cell : row) {
                    if (cell.equals(type)) {
                        accumulator++;
                    }
                }
            }
            return accumulator;
        }

        public int countAdjacentOfType(int x, int y, CellType type) {
            int accumulator = 0;
            for (int i = y - 1; i <= y + 1; ++i) {
                for (int j = x - 1; j <= x + 1; ++j) {
                    if (i == y && j == x) {
                        continue;
                    }
                    if (at(j, i).equals(type)) {
                        accumulator++;
                    }
                }
            }
            return accumulator;
        }

        public void step(CellFunction function) {
            changed = false;
            List<List<CellType>> newGrid = new ArrayList<>();
            for (int y = 0; y < grid.size(); ++y) {
                List<CellType> row = grid.get(y);
                newGrid.add(new ArrayList<>());
                for (int x = 0; x < row.size(); ++x) {
                    CellType cell = row.get(x);
                    CellType result = function.apply(x, y, cell);
                    newGrid.get(y).add(result);
                    if (!cell.equals(result)) {
                        changed = true;
                    }
                }
            }
            grid = newGrid;
        }

        public boolean isChanged() {
            return changed;
        }

        public void print() {
            for (List<CellType> row : grid) {
                for (CellType cell : row) {
                    switch (cell) {
                        case EMPTY:
                            System.out.print('L');
                            break;
                        case OCCUPIED:
                            System.out.print('#');
                            break;
                        case FLOOR:
                            System.out.print('.');
                            break;
                    }
                }
                System.out.print('\n');
            }
        }
    }
}