package pl.piotrmacha.aoc2020.day5;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;

public class Day5Part1 implements Function<String, String> {

    @Override
    public String apply(String input) {
        List<String> codes = InputUtils.splitToStrings(input);

        int maxSeat = 0;

        for (String code : codes) {
            String frontBack = code.substring(0, 7);
            String leftRight = code.substring(7);

            int row = decode(frontBack, 0, 127);
            int column = decode(leftRight, 0, 7);
            int seat = row * 8 + column;

            if (seat > maxSeat) {
                maxSeat = seat;
            }
        }

        return String.valueOf(maxSeat);
    }

    private int decode(String code, int min, int max) {
        int start = min;
        int end = max;

        for (char c : code.toCharArray()) {
            int mid = (start + end) / 2;
            switch (c) {
                case 'F':
                case 'L':
                    end = mid;
                    break;
                case 'B':
                case 'R':
                    start = mid + 1;
                    break;
            }
        }

        if (start != end) {
            throw new RuntimeException("Code is not complete");
        }

        return start;
    }
}