package pl.piotrmacha.aoc2020.day3;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day3Part1 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<String> map = InputUtils.splitToStrings(input);

        int occurrences = 0;

        for (int x = 0, y = 0; y < map.size();) {
            String line = map.get(y);
            char c = line.charAt(x % line.length());

            if (c == '#') {
                occurrences++;
            }

            x += 3;
            y += 1;
        }

        return String.valueOf(occurrences);
    }
}