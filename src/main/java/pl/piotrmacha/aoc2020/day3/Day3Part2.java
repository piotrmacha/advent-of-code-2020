package pl.piotrmacha.aoc2020.day3;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;

public class Day3Part2 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<String> map = InputUtils.splitToStrings(input);

        long result = count(map, 1, 1)
                * count(map, 3, 1)
                * count(map, 5, 1)
                * count(map, 7, 1)
                * count(map, 1, 2);

        return String.valueOf(result);
    }

    private long count(List<String> map, int dx, int dy) {
        long occurrences = 0;

        for (int x = 0, y = 0; y < map.size();) {
            String line = map.get(y);
            char c = line.charAt(x % line.length());

            if (c == '#') {
                occurrences++;
            }

            x += dx;
            y += dy;
        }

        return occurrences;
    }
}