package pl.piotrmacha.aoc2020.day7;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.*;
import java.util.function.Function;

public class Day7Part2 implements Function<String, String> {
    private final Map<String, Bag> bags = new HashMap<>();

    @Override
    public String apply(String input) {
        loadInput(input);

        long result = bags.get("shiny gold").countBagsInside();

        return String.valueOf(result);
    }

    private void loadInput(String input) {
        List<String> lines = InputUtils.splitToStrings(input);
        for (String line : lines) {
            String[] parts = line.split(" ", 5);
            String name = parts[0] + " " + parts[1];
            String rules = parts[4];
            Bag bag = new Bag(name);
            parseLinks(bag, rules);
            bags.put(name, bag);
        }
    }

    private void parseLinks(Bag bag, String rules) {
        if ("no other bags.".equals(rules)) {
            return;
        }

        String[] rulesParts = rules.split(",");
        for (String rule : rulesParts) {
            String[] parts = rule.trim().split(" ");
            int amount = Integer.parseInt(parts[0]);
            String name = parts[1] + " " + parts[2];
            bag.addLink(name, amount);
        }
    }

    private class Bag {
        public String name;
        public Set<Link> links = new HashSet<>();

        public Bag(String name) {
            this.name = name;
        }

        public void addLink(String name, int amount) {
            links.add(new Link(name, amount));
        }

        public long countBagsInside() {
            long accumulator = 0;
            Queue<Link> toVisit = new ArrayDeque<>();
            toVisit.add(new Link(name, 1));

            while (!toVisit.isEmpty()) {
                Link currentLink = toVisit.poll();
                Bag currentBag = bags.get(currentLink.name);
                for (Link link : currentBag.links) {
                    accumulator += link.amount * currentLink.amount;
                    toVisit.add(new Link(link.name, link.amount * currentLink.amount));
                }
            }

            return accumulator;
        }

        private class Link {
            public String name;
            public Integer amount;

            public Link(String name, Integer amount) {
                this.name = name;
                this.amount = amount;
            }
        }
    }
}