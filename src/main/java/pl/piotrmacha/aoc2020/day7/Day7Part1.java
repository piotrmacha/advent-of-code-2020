package pl.piotrmacha.aoc2020.day7;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.*;
import java.util.function.Function;

import static java.util.Arrays.*;
import static java.util.stream.Collectors.toList;

public class Day7Part1 implements Function<String, String> {
    private final Map<String, Bag> bags = new HashMap<>();

    @Override
    public String apply(String input) {
        loadInput(input);

        Set<Bag> possibleHolders = new HashSet<>();
        for (Bag bag : bags.values()) {
            if (bag.canHold("shiny gold")) {
                possibleHolders.add(bag);
            }
        }

        return String.valueOf(possibleHolders.size());
    }

    private void loadInput(String input) {
        List<String> lines = InputUtils.splitToStrings(input);
        for (String line : lines) {
            String[] parts = line.split(" ", 5);
            String name = parts[0] + " " + parts[1];
            String rules = parts[4];
            Bag bag = new Bag(name);
            parseLinks(bag, rules);
            bags.put(name, bag);
        }
    }

    private void parseLinks(Bag bag, String rules) {
        if ("no other bags.".equals(rules)) {
            return;
        }

        String[] rulesParts = rules.split(",");
        for (String rule : rulesParts) {
            String[] parts = rule.trim().split(" ");
            int amount = Integer.parseInt(parts[0]);
            String name = parts[1] + " " + parts[2];
            bag.addLink(name, amount);
        }
    }

    private class Bag {
        public String name;
        public Set<Link> links = new HashSet<>();

        public Bag(String name) {
            this.name = name;
        }

        public void addLink(String name, int amount) {
            links.add(new Link(name, amount));
        }

        public boolean canHold(String searchedBag) {
            Set<String> visited = new HashSet<>();
            Queue<Bag> toVisit = new ArrayDeque<>();
            toVisit.add(this);

            while (!toVisit.isEmpty()) {
                Bag currentBag = toVisit.poll();
                for (Link link : currentBag.links) {
                    if (link.name.equals(searchedBag)) {
                        return true;
                    }

                    if (!visited.contains(link.name)) {
                        toVisit.add(bags.get(link.name));
                    }
                }
                visited.add(currentBag.name);
            }

            return false;
        }

        private class Link {
            public String name;
            public Integer amount;

            public Link(String name, Integer amount) {
                this.name = name;
                this.amount = amount;
            }
        }
    }
}