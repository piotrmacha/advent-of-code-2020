package pl.piotrmacha.aoc2020.day6;

import java.util.*;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public class Day6Part2 implements Function<String, String> {

    @Override
    public String apply(String input) {
        List<List<String>> groups = stream(input.split("\n\n"))
                .map(group -> asList(group.split("\n")))
                .collect(toList());

        int result = 0;

        for (List<String> group : groups) {
            Map<Integer, Integer> yesAnswers = new HashMap<>();
            for (String person : group) {
                person.trim().chars().forEach(c -> {
                    if (!yesAnswers.containsKey(c)) {
                        yesAnswers.put(c, 0);
                    }
                    yesAnswers.put(c, yesAnswers.get(c) + 1);
                });
            }
            result += yesAnswers.values().stream()
                    .filter(s -> s == group.size())
                    .count();
        }

        return String.valueOf(result);
    }
}