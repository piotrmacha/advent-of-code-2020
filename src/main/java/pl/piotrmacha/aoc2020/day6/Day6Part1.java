package pl.piotrmacha.aoc2020.day6;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

public class Day6Part1 implements Function<String, String> {

    @Override
    public String apply(String input) {
        List<List<String>> groups = stream(input.split("\n\n"))
                .map(group -> asList(group.split("\n")))
                .collect(toList());

        int result = 0;

        for (List<String> group : groups) {
            Set<Integer> yesAnswers = new HashSet<>();
            for (String person : group) {
                person.chars().forEach(yesAnswers::add);
            }
            result += yesAnswers.size();
        }

        return String.valueOf(result);
    }
}