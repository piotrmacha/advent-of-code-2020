package pl.piotrmacha.aoc2020;

import java.util.List;

import static java.util.stream.Collectors.toList;

public abstract class InputUtils {
    public static List<String> splitToStrings(String input) {
        return List.of(input.split("\n")).stream()
                .map(String::trim)
                .collect(toList());
    }

    public static List<Integer> splitToInts(String input) {
        return splitToStrings(input).stream()
                .map(Integer::parseInt)
                .collect(toList());
    }

    public static List<Long> splitToLongs(String input) {
        return splitToStrings(input).stream()
                .map(Long::parseLong)
                .collect(toList());
    }
}
