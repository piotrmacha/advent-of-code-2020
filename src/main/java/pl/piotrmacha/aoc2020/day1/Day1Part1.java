package pl.piotrmacha.aoc2020.day1;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;

public class Day1Part1 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<Integer> expenses = InputUtils.splitToInts(input);

        for (int i = 0; i < expenses.size(); ++i) {
            for (int j = i; j < expenses.size(); ++j) {
                int a = expenses.get(i);
                int b = expenses.get(j);

                if (a + b == 2020) {
                    return String.valueOf(a * b);
                }
            }
        }

        throw new RuntimeException("Can not find result");
    }
}
