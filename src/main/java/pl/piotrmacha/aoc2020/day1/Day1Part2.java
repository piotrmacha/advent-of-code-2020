package pl.piotrmacha.aoc2020.day1;

import pl.piotrmacha.aoc2020.InputUtils;

import java.util.List;
import java.util.function.Function;

public class Day1Part2 implements Function<String, String> {
    @Override
    public String apply(String input) {
        List<Integer> expenses = InputUtils.splitToInts(input);

        for (int i = 0; i < expenses.size(); ++i) {
            for (int j = i; j < expenses.size(); ++j) {
                for (int k = j; k < expenses.size(); ++k) {
                    int a = expenses.get(i);
                    int b = expenses.get(j);
                    int c = expenses.get(k);

                    if (a + b + c == 2020) {
                        return String.valueOf(a * b * c);
                    }
                }
            }
        }

        throw new RuntimeException("Can not find result");
    }
}
