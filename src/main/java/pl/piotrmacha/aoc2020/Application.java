package pl.piotrmacha.aoc2020;

import pl.piotrmacha.aoc2020.day1.Day1Part1;
import pl.piotrmacha.aoc2020.day1.Day1Part2;
import pl.piotrmacha.aoc2020.day10.Day10Part1;
import pl.piotrmacha.aoc2020.day10.Day10Part2;
import pl.piotrmacha.aoc2020.day11.Day11Part1;
import pl.piotrmacha.aoc2020.day11.Day11Part2;
import pl.piotrmacha.aoc2020.day12.Day12Part1;
import pl.piotrmacha.aoc2020.day2.Day2Part1;
import pl.piotrmacha.aoc2020.day2.Day2Part2;
import pl.piotrmacha.aoc2020.day3.Day3Part1;
import pl.piotrmacha.aoc2020.day3.Day3Part2;
import pl.piotrmacha.aoc2020.day4.Day4Part1;
import pl.piotrmacha.aoc2020.day4.Day4Part2;
import pl.piotrmacha.aoc2020.day5.Day5Part1;
import pl.piotrmacha.aoc2020.day5.Day5Part2;
import pl.piotrmacha.aoc2020.day6.Day6Part1;
import pl.piotrmacha.aoc2020.day6.Day6Part2;
import pl.piotrmacha.aoc2020.day7.Day7Part1;
import pl.piotrmacha.aoc2020.day7.Day7Part2;
import pl.piotrmacha.aoc2020.day8.Day8Part1;
import pl.piotrmacha.aoc2020.day8.Day8Part2;
import pl.piotrmacha.aoc2020.day9.Day9Part1;
import pl.piotrmacha.aoc2020.day9.Day9Part2;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.lang.String.format;

public class Application {
    private final List<TaskDefinition> tasks = createTaskDefinitions();

    private List<TaskDefinition> createTaskDefinitions() {
        List<TaskDefinition> tasks = new ArrayList<>();
        tasks.add(new TaskDefinition("day1.txt", new Day1Part1()));
        tasks.add(new TaskDefinition("day1.txt", new Day1Part2()));
        tasks.add(new TaskDefinition("day2.txt", new Day2Part1()));
        tasks.add(new TaskDefinition("day2.txt", new Day2Part2()));
        tasks.add(new TaskDefinition("day3.txt", new Day3Part1()));
        tasks.add(new TaskDefinition("day3.txt", new Day3Part2()));
        tasks.add(new TaskDefinition("day4.txt", new Day4Part1()));
        tasks.add(new TaskDefinition("day4.txt", new Day4Part2()));
        tasks.add(new TaskDefinition("day5.txt", new Day5Part1()));
        tasks.add(new TaskDefinition("day5.txt", new Day5Part2()));
        tasks.add(new TaskDefinition("day6.txt", new Day6Part1()));
        tasks.add(new TaskDefinition("day6.txt", new Day6Part2()));
        tasks.add(new TaskDefinition("day7.txt", new Day7Part1()));
        tasks.add(new TaskDefinition("day7.txt", new Day7Part2()));
        tasks.add(new TaskDefinition("day8.txt", new Day8Part1()));
        tasks.add(new TaskDefinition("day8.txt", new Day8Part2()));
        tasks.add(new TaskDefinition("day9.txt", new Day9Part1()));
        tasks.add(new TaskDefinition("day9.txt", new Day9Part2()));
        tasks.add(new TaskDefinition("day10.txt", new Day10Part1()));
        tasks.add(new TaskDefinition("day10.txt", new Day10Part2()));
        tasks.add(new TaskDefinition("day11.txt", new Day11Part1()));
        tasks.add(new TaskDefinition("day11.txt", new Day11Part2()));
        tasks.add(new TaskDefinition("day12.txt", new Day12Part1()));
        return tasks;
    }

    private void run(String[] args) {
        if (args.length > 0) {
            for (String arg : args) {
                tasks.stream()
                        .filter(task -> task.getName().equals(arg))
                        .findAny()
                        .ifPresentOrElse(
                                this::executeTask,
                                () -> {
                                    throw new RuntimeException(format("Can not find task %s", arg));
                                }
                        );
            }
            return;
        }
        tasks.forEach(this::executeTask);
    }

    private void executeTask(TaskDefinition task) {
        System.out.println("-------------------------------------------------------------");
        System.out.printf("Executing: %s%n", task.getName());

        InputStream inputFile = getClass().getClassLoader().getResourceAsStream(task.getInput());
        if (inputFile == null) {
            throw new RuntimeException(format("ERROR: Can not find resource %s", task.getInput()));
        }

        try {
            String input = new String(inputFile.readAllBytes());

            long startTime = System.nanoTime();
            String output = task.getExecutor().apply(input);
            long deltaTime = System.nanoTime() - startTime;

            System.out.printf("Output: %s%n", output);
            System.out.printf("Time: %.4f s%n", deltaTime / 1000000000.0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        new Application().run(args);
    }

    private static class TaskDefinition {
        private final String input;
        private final Function<String, String> executor;

        public TaskDefinition(String input, Function<String, String> executor) {
            this.input = input;
            this.executor = executor;
        }

        public String getName() {
            return getExecutor().getClass().getSimpleName();
        }

        public String getInput() {
            return input;
        }

        public Function<String, String> getExecutor() {
            return executor;
        }
    }
}
